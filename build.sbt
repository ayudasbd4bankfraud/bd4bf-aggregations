name := "bd4bf-aggregations"

organization := "com.paradigma.bd4bf"

version := "1.0.1"

scalaVersion in ThisBuild := "2.10.5"

parallelExecution in test := false

isSnapshot := true

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-compiler" % "2.10.5",
  "org.apache.spark" % "spark-core_2.10" % "1.5.0",
  "org.scalatest" % "scalatest_2.10" % "2.0" % "test",
  "org.apache.commons" % "commons-lang3" % "3.3.2",
  "org.slf4j" % "slf4j-api" % "1.7.2"
)

publishTo := Some(Resolver.file("file",  new File(Path.userHome.absolutePath+"/.m2/repository")))
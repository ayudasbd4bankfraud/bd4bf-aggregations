package com.paradigma.aggregations.domain

/**
 * Created by paradigma
 */
case class MonthlyAggregation(
                          var amountOverMonth: Double,
                          var averageDailyOverMonth: Double,
                          var amountMerchantTypeOverMonth: Map[String, Double],
                          var numberMerchantTypeOverMonth: Map[String, Int],
                          var amountSameCountryOverMonth: Map[String, Double],
                          var numberSameCountryOverMonth: Map[String, Int]) extends Serializable {
}

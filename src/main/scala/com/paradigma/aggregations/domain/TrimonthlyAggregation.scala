package com.paradigma.aggregations.domain

/**
 * Created by paradigma
 */
case class TrimonthlyAggregation(
                             var averageOverThreeMonths: Double,
                             var amountMerchantTypeOverThreeMonths: Map[String, Double]) extends Serializable {
}

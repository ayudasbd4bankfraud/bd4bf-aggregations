package com.paradigma.aggregations.domain

/**
 * Created by paradigma
 */
class Transaction(
                        val ccnumber: String,
                        val txnid: String,
                        val amount: Double,
                        val date: Long,
                        val ecommerce: Int,
                        val foreign: Int,
                        val fraud: Int,
                        val merchant: String,
                        val country: String
                        ) extends Serializable {
}

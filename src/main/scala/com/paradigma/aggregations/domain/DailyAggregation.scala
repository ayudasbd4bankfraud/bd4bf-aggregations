package com.paradigma.aggregations.domain

/**
 * Created by paradigma
 */
case class DailyAggregation(
                        var amountSameDay: Double,
                       var numberSameDay: Long) extends Serializable {
}
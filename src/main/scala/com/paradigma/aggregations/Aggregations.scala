package com.paradigma.aggregations

import com.paradigma.aggregations.domain._

import scala.collection.mutable

/**
 * Created by paradigma
 */
object Aggregations {

  val MerchantCodesAmounts =
    mutable.Map[String, Double](("0742", 0.0), ("0763", 0.0), ("1520", 0.0), ("0780", 0.0))
  val MerchantCodesCounts =
    mutable.Map[String, Int](("0742", 0), ("0763", 0), ("1520", 0), ("0780", 0))

  val MerchantCountriesAmounts =
    mutable.Map[String, Double](("FR", 0.0),("US", 0.0),("ES", 0.0), ("GB", 0.0), ("GR", 0.0), ("CH", 0.0))
  val MerchantCountriesCounts =
    mutable.Map[String, Int](("FR", 0),("US", 0), ("ES", 0), ("GB", 0), ("GR", 0), ("CH", 0))

  def calculateDailyAggregations(txns: Seq[Transaction]): DailyAggregation = {

    // Amount same day: Total amount spent with a credit card on the day of a given transaction. Here, for each
    // transaction we computed the total amount spent in a day with that credit card
    val amountSameDay = txns.map(_.amount).sum

    // Number same day: Total number of transactions on the day of a given transaction. For this attribute, we computed
    // the total number of transactions in a day with that credit card
    val numberSameDay = txns.size

    new DailyAggregation(amountSameDay, numberSameDay)
  }

  def calculateMonthlyAggregations(txns: Seq[Transaction]): MonthlyAggregation = {
    // MONTHLY

    val oneMonthTxnAmounts = txns.map(_.amount)

    // Txn amount over month: Average spending per transaction over a 30-day period on all transactions till this transaction.
    // We computed the total amount spent with a credit card during past 30 days prior to a transaction
    // and divided it by the number of transactions to get the average amount spent
    val amountOverMonth = if (oneMonthTxnAmounts.nonEmpty)  oneMonthTxnAmounts.sum / oneMonthTxnAmounts.size else 0.0

    // Average daily over month: Average spending per day over past 30 days before this transaction.
    // We calculated the total amount spent with a credit card during past 30 days prior to a transaction and divided it
    // by 30 to compute the average daily spent over a month prior to a transaction
    val averageDailyOverMonth = oneMonthTxnAmounts.sum / 30

    // Amount merchant type over month: Average spending per day on a merchant type (based on merchant code) over a
    // 30-day period for each transaction. In this case, we first computed the total amount spent with a credit card on
    // a specific merchant type during past 30 days prior to a transaction and then divided this sum by 30 to get the
    // average money spent with a specific merchant type over a month prior to a transaction
    val amountMerchantTypeOverMonth = MerchantCodesAmounts.clone
    txns.groupBy(_.merchant).mapValues(_.map(_.amount / 30).sum).foreach(tuple => {
      amountMerchantTypeOverMonth.update(tuple._1, tuple._2)
    })
    //    amountMerchantTypeOverMonthMap.foreach(x => println(x._2))

    // Number merchant type over month: Total number of transactions with the same merchant over a period of 30 days
    // before a given transaction. For this attribute, we computed the total number of transactions with a credit card
    // with a specific merchant type during past 30 days prior to a transaction
    val numberMerchantTypeOverMonth = MerchantCodesCounts.clone
    txns.groupBy(_.merchant).mapValues(_.map(_ => 1).sum).foreach(tuple => {
      numberMerchantTypeOverMonth.update(tuple._1, tuple._2)
    })
    //    numberMerchantTypeOverMonthMap.foreach(x => println(x._2))

    // Amount same country over month: Average amount spent over a 30-day period on all transactions up to this
    // transaction in the same country. For this attribute, we first computed the total amount spent with a credit card
    // in a specific country during past 30 days prior to a transaction and then divided this sum by 30 to get the
    // average money spent in a specific country over a month prior to a transaction
    val amountSameCountryOverMonth = MerchantCountriesAmounts.clone
    txns.groupBy(_.country).mapValues(_.map(_.amount / 30).sum).foreach(tuple => {
      amountSameCountryOverMonth.update(tuple._1, tuple._2)
    })
    //    amountSameCountryOverMonthMap.foreach(x => println(x._2))

    // Number same country over month: Total number of transactions in the same country during past 30 days before this
    // transaction. In this case, we computed the total number of transactions with a credit card in a specific country
    // during past 30 days prior to a transaction
    val numberSameCountryOverMonthMap = MerchantCountriesCounts.clone
    txns.groupBy(_.country).mapValues(_.map(_ => 1).sum).foreach(tuple => {
      numberSameCountryOverMonthMap.update(tuple._1, tuple._2)
    })
    //    numberSameCountryOverMonthMap.foreach(x => println(x._2))

    new MonthlyAggregation(
      amountOverMonth, averageDailyOverMonth, amountMerchantTypeOverMonth.toMap,
      numberMerchantTypeOverMonth.toMap, amountSameCountryOverMonth.toMap, numberSameCountryOverMonthMap.toMap)
  }

  def calculateTrimonthlyAggregations(txns: Seq[Transaction]): TrimonthlyAggregation = {
    // TRIMONTHLY

    // Average over three months: Average over 3 months: Average amount spent over the course of 1 week during past 3
    // months. For this attribute, we computed the total amount spent with a credit card during past 90 days prior to a
    // transaction, and then divided it by 12 to get the average weekly spent over three months
    val averageOverThreeMonths = txns.map(_.amount).sum / 12

    // Amount merchant type over three months: Average weekly spending on a merchant type during past 3 months before a
    // given transaction. For this attribute, we computed the total amount spent with a credit card on a specific
    // merchant type during past 90 days prior to a transaction, and then divided it by 12 to get the average weekly
    // amount spent over three months on that merchant type
    val amountMerchantTypeOverThreeMonthsMap = MerchantCodesAmounts.clone
    txns.groupBy(_.merchant).mapValues(_.map(_.amount / 12).sum).foreach(tuple => {
      amountMerchantTypeOverThreeMonthsMap.update(tuple._1, tuple._2)
    })

    new TrimonthlyAggregation(averageOverThreeMonths, amountMerchantTypeOverThreeMonthsMap.toMap)
  }

}
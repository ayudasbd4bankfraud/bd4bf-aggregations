package com.paradigma.aggregations

import java.time.Instant

import com.paradigma.aggregations.domain.Transaction
import org.scalatest.{FlatSpec, Matchers}

/**
 * Created by paradigma
 */
class TrimonthlyAggregationsTest extends FlatSpec with Matchers {

  "A 'calculateTrimonthlyAggregations' method execution with 5 txns" should "return a TrimonthlyAggregation object" in {
    // prepare test data
    val now = Instant.now().getEpochSecond
    val txn1 = new Transaction("cc01", "txn1", 100, now, 0, 0, 0, "0742", "ES")
    val txn2 = new Transaction("cc02", "txn2", 625.50, now, 0, 0, 0, "0742", "ES")
    val txn3 = new Transaction("cc03", "txn3", 500.50, now, 0, 0, 0, "0742", "ES")
    val txn4 = new Transaction("cc04", "txn4", 500.50, now, 0, 0, 0, "0763", "ES")
    val txn5 = new Transaction("cc05", "txn5", 500.50, now, 0, 0, 0,  "0763", "ES")
    val txns = List[Transaction](txn1, txn2, txn3, txn4, txn5)

    // execute
    val trimonthlyAggs = Aggregations.calculateTrimonthlyAggregations(txns)

    // assert
    trimonthlyAggs.amountMerchantTypeOverThreeMonths.getOrElse("0742", None) should be(102.16666666666667)
    trimonthlyAggs.amountMerchantTypeOverThreeMonths.getOrElse("0763", None) should be(83.41666666666667)
    trimonthlyAggs.amountMerchantTypeOverThreeMonths.getOrElse("1520", None) should be(0.0)
    trimonthlyAggs.amountMerchantTypeOverThreeMonths.getOrElse("0780", None) should be(0.0)
    trimonthlyAggs.averageOverThreeMonths should be(185.58333333333333)
  }

  "A 'calculateTrimonthlyAggregations' method execution with no txns" should "return a TrimonthlyAggregation object" in {
    // prepare test data
    val txns = List[Transaction]()

    // execute
    val trimonthlyAggs = Aggregations.calculateTrimonthlyAggregations(txns)

    // assert
    trimonthlyAggs.amountMerchantTypeOverThreeMonths.getOrElse("0742", None) should be(0.0)
    trimonthlyAggs.amountMerchantTypeOverThreeMonths.getOrElse("0763", None) should be(0.0)
    trimonthlyAggs.amountMerchantTypeOverThreeMonths.getOrElse("1520", None) should be(0.0)
    trimonthlyAggs.amountMerchantTypeOverThreeMonths.getOrElse("0780", None) should be(0.0)
    trimonthlyAggs.averageOverThreeMonths should be(0.0)
  }

}
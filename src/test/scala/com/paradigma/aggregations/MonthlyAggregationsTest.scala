package com.paradigma.aggregations

import java.time.Instant

import com.paradigma.aggregations.domain.Transaction
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by paradigma
  */
class MonthlyAggregationsTest extends FlatSpec with Matchers {

  "A 'calculateMonthlyAggregations' method execution with 5 txns" should "return a MonthlyAggregation object" in {
    // prepare test data
    val now = Instant.now().getEpochSecond
    val txn1 = new Transaction("cc01", "txn1", 500.50, now, 0, 0, 0, "0742", "ES")
    val txn2 = new Transaction("cc02", "txn2", 500.50, now, 0, 0, 0, "0742", "ES")
    val txn3 = new Transaction("cc03", "txn3", 500.50, now, 0, 0, 0, "0763", "ES")
    val txn4 = new Transaction("cc04", "txn4", 500.50, now, 0, 0, 0, "0763", "ES")
    val txn5 = new Transaction("cc05", "txn5", 500.50, now, 0, 0, 0, "0763", "ES")
    val txns = Seq[Transaction](txn1, txn2, txn3, txn4, txn5)

    // execute
    val monthlyAggs = Aggregations.calculateMonthlyAggregations(txns)

    // assert
    monthlyAggs.amountMerchantTypeOverMonth.getOrElse("0742", None) should be(33.36666666666667)
    monthlyAggs.amountMerchantTypeOverMonth.getOrElse("0763", None) should be(50.05)
    monthlyAggs.amountMerchantTypeOverMonth.getOrElse("1520", None) should be(0.0)
    monthlyAggs.amountMerchantTypeOverMonth.getOrElse("0780", None) should be(0.0)

    monthlyAggs.amountSameCountryOverMonth.getOrElse("ES", None) should be(83.41666666666667)
    monthlyAggs.amountSameCountryOverMonth.getOrElse("US", None) should be(0.0)
    monthlyAggs.amountSameCountryOverMonth.getOrElse("FR", None) should be(0.0)
    monthlyAggs.amountSameCountryOverMonth.getOrElse("GB", None) should be(0.0)
    monthlyAggs.amountSameCountryOverMonth.getOrElse("GR", None) should be(0.0)
    monthlyAggs.amountSameCountryOverMonth.getOrElse("CH", None) should be(0.0)

    monthlyAggs.averageDailyOverMonth should be(83.41666666666667)

    monthlyAggs.numberMerchantTypeOverMonth.getOrElse("0742", None) should be(2)
    monthlyAggs.numberMerchantTypeOverMonth.getOrElse("0763", None) should be(3)
    monthlyAggs.numberMerchantTypeOverMonth.getOrElse("1520", None) should be(0)
    monthlyAggs.numberMerchantTypeOverMonth.getOrElse("0780", None) should be(0)

    monthlyAggs.numberSameCountryOverMonth.getOrElse("ES", None) should be(5)
    monthlyAggs.numberSameCountryOverMonth.getOrElse("US", None) should be(0)
    monthlyAggs.numberSameCountryOverMonth.getOrElse("FR", None) should be(0)
    monthlyAggs.numberSameCountryOverMonth.getOrElse("GB", None) should be(0)
    monthlyAggs.numberSameCountryOverMonth.getOrElse("GR", None) should be(0)
    monthlyAggs.numberSameCountryOverMonth.getOrElse("CH", None) should be(0)

    monthlyAggs.amountOverMonth should be(500.50)

  }

  "A 'calculateMonthlyAggregations' method execution with no txns" should "return a MonthlyAggregation object" in {
    // prepare test data
    val txns = List[Transaction]()

    // execute
    val monthlyAggs = Aggregations.calculateMonthlyAggregations(txns)

    // assert
    monthlyAggs.amountMerchantTypeOverMonth.getOrElse("0742", None) should be(0.0)
    monthlyAggs.amountMerchantTypeOverMonth.getOrElse("0763", None) should be(0.0)
    monthlyAggs.amountMerchantTypeOverMonth.getOrElse("1520", None) should be(0.0)
    monthlyAggs.amountMerchantTypeOverMonth.getOrElse("0780", None) should be(0.0)

    monthlyAggs.amountSameCountryOverMonth.getOrElse("ES", None) should be(0.0)
    monthlyAggs.amountSameCountryOverMonth.getOrElse("US", None) should be(0.0)
    monthlyAggs.amountSameCountryOverMonth.getOrElse("FR", None) should be(0.0)
    monthlyAggs.amountSameCountryOverMonth.getOrElse("GB", None) should be(0.0)
    monthlyAggs.amountSameCountryOverMonth.getOrElse("GR", None) should be(0.0)
    monthlyAggs.amountSameCountryOverMonth.getOrElse("CH", None) should be(0.0)

    monthlyAggs.averageDailyOverMonth should be(0.0)

    monthlyAggs.numberMerchantTypeOverMonth.getOrElse("0742", None) should be(0)
    monthlyAggs.numberMerchantTypeOverMonth.getOrElse("0763", None) should be(0)
    monthlyAggs.numberMerchantTypeOverMonth.getOrElse("1520", None) should be(0)
    monthlyAggs.numberMerchantTypeOverMonth.getOrElse("0780", None) should be(0)

    monthlyAggs.numberSameCountryOverMonth.getOrElse("ES", None) should be(0)
    monthlyAggs.numberSameCountryOverMonth.getOrElse("US", None) should be(0)
    monthlyAggs.numberSameCountryOverMonth.getOrElse("FR", None) should be(0)
    monthlyAggs.numberSameCountryOverMonth.getOrElse("GB", None) should be(0)
    monthlyAggs.numberSameCountryOverMonth.getOrElse("GR", None) should be(0)
    monthlyAggs.numberSameCountryOverMonth.getOrElse("CH", None) should be(0)

    monthlyAggs.amountOverMonth should be(0.0)

  }

}
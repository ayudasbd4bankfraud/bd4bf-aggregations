package com.paradigma.aggregations

import java.time.Instant

import com.paradigma.aggregations.domain.Transaction
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
 * Created by paradigma
 */
class DailyAggregationsTest extends FlatSpec with Matchers with BeforeAndAfter {

  "A 'calculateDailyAggregations' method execution with 5 txns" should "return a DailyAggregation object" in {
    // prepare test data
    val now = Instant.now().getEpochSecond
    val txn1 = new Transaction("cc01", "txn1", 500.50, now, 0, 0, 0, "0742", "ES")
    val txn2 = new Transaction("cc02", "txn2", 500.50, now, 0, 0, 0, "0742", "ES")
    val txn3 = new Transaction("cc03", "txn3", 500.50, now, 0, 0, 0, "0742", "ES")
    val txn4 = new Transaction("cc04", "txn4", 500.50, now, 0, 0, 0, "0742", "ES")
    val txn5 = new Transaction("cc05", "txn5", 500.50, now, 0, 0, 0, "0742", "ES")
    val txns = Seq[Transaction](txn1, txn2, txn3, txn4, txn5)

    // execute
    val dailyAggs = Aggregations.calculateDailyAggregations(txns)

    // assert
    dailyAggs.amountSameDay should be(2502.5)
    dailyAggs.numberSameDay should be(5)
  }

  "A 'calculateDailyAggregations' method execution with no txns" should "return a DailyAggregation object" in {
    // prepare test data
    val txns = Seq[Transaction]()

    // execute
    val dailyAggs = Aggregations.calculateDailyAggregations(txns)

    // assert
    dailyAggs.amountSameDay should be(0.0)
    dailyAggs.numberSameDay should be(0)
  }

}